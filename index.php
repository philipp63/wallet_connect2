<?php
require_once 'core/function.php';

?>
<!doctype html>
<html lang="fr">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/build/style.css" rel="stylesheet">
    <title>Wallet Connect</title>
</head>
<body class="accueil">


<div class="container-fluid mountain">

    <svg class="green"
         id="Calque_2"
         data-name="Calque 2"
         xmlns="http://www.w3.org/2000/svg"
         viewBox="0 0 1662 877.08">
        <defs>
            <style>.cls-4 {
                    fill: #7de77d;
                }

                .cls-3 {
                    fill: #438143;
                }</style>
        </defs>
        <title>mountaingreen</title>
        <path class="cls-3" d="M1920,215s-381-87-532,206S588,1080,258,1080l1662-43Z"
              transform="translate(-258 -202.92)"/>
        <path class="cls-4" d="M1920,258s-381-87-532,206S588,1080,258,1080H1920Z" transform="translate(-258 -202.92)"/>
    </svg>

    <svg class="blue"
         id="Calque_1"
         data-name="Calque 1"
         xmlns="http://www.w3.org/2000/svg"
         viewBox="0 0 1921 711.06">
        <defs>
            <style>.cls-2 {
                    fill: cyan;
                }
                .cls-1 {
                    fill: #45B2FA;
                }</style>
        </defs>
        <title>mountainblue</title>
        <path class="cls-1" d="M66,398.88S788.11,302,965.74,525.68,1655,865.32,1920.5,865.32V1022H66Z"
              transform="translate(0.5 -368.44)"/>
        <path class="cls-2" d="M-.5,391.5s748-107,932,140,714,375,989,375v173H-.5Z" transform="translate(0.5 -368.44)"/>
    </svg>

</div>

<div class="container-xl mt-5">
    <img src="assets/images/wallet-connectlogo.png" alt="">
    <div class="info col-7 mt-3">
        <h2>Vos fichiers <br> à portée de mains !</h2>
        <p>Où que vous soyez, à n'importe quel instant,
            piéces d'identité, factures, contrats, fiches de paye...</p>
        <h2>Fini les documents qui s'entassent !</h2>
        <p>Centralisé et surtout sécurisé, plus besoin de boites
            et de cartons, Wallet Connect vous fait gagner du temps
            et <strong>vous libère l'esprit</strong></p>
        <div class="text-center pt-3">

            <button type="button"
                    class="btn btn-primary"
                    data-bs-toggle="modal"
                    data-bs-target="#connect">
                Connexion
            </button>
            <?php getModalConnection() ?>
<!--            <a href="login.php" type="submit" class="btn btn-primary">Connexion</a>-->

            <button type="button"
                    class="btn btn-success"
                    data-bs-toggle="modal"
                    data-bs-target="#inscription">
                Inscription
            </button>
            <?php getModalInscription() ?>
<!--            <a href="login.php" type="submit" class="btn btn-success">Inscription</a>-->

    </div>
</div>
    <footer>

        <?php getFooter();?>
    </footer>



<!--<script src="/assets/js/jquery.min.js"></script>-->
<script src="/assets/js/bootstrap.min.js"></script>

<script type="javascript">
    let myConnect = document.querySelector('#inscription,#connect')

    let myInput = document.querySelectorAll('myInput')
    myConnect.addEventListener('shown.bs.modal', ()=> {
        myInput.focus()
    });

</script>

</body>
</html>








