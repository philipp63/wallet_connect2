<?php
require_once 'core/function.php';

if(!empty($_POST['login']) && !empty($_POST['password'])) {
    $db = getDatabaseConnexion();
    $req = $db->prepare('SELECT * FROM `user` WHERE email = :email LIMIT 1');
    $req->execute([
        'email' => $_POST['login'],
    ]);

    if(false === $user = $req->fetchObject()) {
        header('Location: /login.php?status=error_login');
    } else {
        if(password_verify($_POST['password'], $user->passwd)) {
            $_SESSION['logged_in'] = true;

            if(!empty($_GET['from'])) {
                header('Location: '.$_GET['from']);
            } else {
                header('Location: /organiser.php');
            }

        } else {
            header('Location: /login.php?status=error_login');
        }
    }
}
?>

<!--Modal Connexion-->
<div class="modal fade"
     id="connect"
     tabindex="-1"
     aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog text-lg-start">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"
                    id="connect">Connexion</h5>
                <button type="button"
                        class="btn-close"
                        data-bs-dismiss="modal"
                        aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Email </label>
                        <input type="email"
                               class="form-control"
                               id="exampleInputEmail1"
                               aria-describedby="emailHelp">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputPassword1"
                               class="form-label">Mot de passe</label>
                        <input type="password"
                               class="form-control"
                               id="exampleInputPassword1">
                    </div>
                    <div class="mb-3 form-check">
                        <input type="checkbox"
                               class="form-check-input"
                               id="exampleCheck1">
                        <label class="form-check-label"
                               for="exampleCheck1">Check me out</label>
                    </div>
<!--                    <button type="submit"-->
<!--                            class="btn btn-primary">Connexion</button>-->
                                <a href="organiser.php" type="submit" class="btn btn-primary">Connexion</a>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button"
                        class="btn btn-secondary"
                        data-bs-dismiss="modal">Fermer</button>
            </div>
        </div>
    </div>
</div>

