<?php
require '../../core/function.php';
getHeader('');

use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;

$filesystem = new Filesystem();

try {
    $filesystem->mkdir(sys_get_temp_dir().'/'.random_int(0, 1000));
} catch (IOExceptionInterface $exception) {
    echo "An error occurred while creating your directory at ".$exception->getPath();
}

?>

<html lang="en">
    <div class="col-4 text-start mt-5">
        <select class="form-select" aria-label="Default select example">
            <option selected>Choisissez votre catégorie</option>
            <option value="1">Administration</option>
            <option value="2">Factures</option>
            <option value="3">Contrats</option>
            <option value="4">Identité</option>
            <option value="5">Assurance</option>
        </select>
        <form action="organiser.php" method="post">
            <p>Veuillez nommer votre document : <input type="text" name="fileName" /></p>
            <button type="" class="btn btn-primary"></button>
        </form>
    </div>


</html>

<?php getFooter();?>
