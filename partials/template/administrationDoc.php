<?php
require '../../core/function.php';
getHeader('administrationDoc');

?>

<div class="container">
    <div class="col-12">
        <div class="text-center"><h2>Documents Administratifs</h2> </div>
        <div class="d-flex row-cols-sm-auto mt-6">
            <?php getCard();  ?>
            <?php getCard();  ?>
            <?php getCard();  ?>
            <?php getCard();  ?>
            <?php getCard();  ?>
            <?php getCard();  ?>
        </div>

        <div class="d-flex row-cols-sm-auto mt-6">
            <?php getCard();  ?>
            <?php getCard();  ?>
            <?php getCard();  ?>
            <?php getCard();  ?>
            <?php getCard();  ?>
            <?php getCard();  ?>
        </div>
    </div>
</div>

<?php getFooter();?>
