<?php

?>
<!doctype html>
<html lang="fr">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/build/style.css" rel="stylesheet">
    <title>Wallet Connect</title>
</head>
<body>
<div class="container">
    <nav class="navbar navbar-expand-lg navbar-light ">
        <div class="container-fluid col-12">
            <img class="imglogo" src="../assets/images/wallet-connectlogo.png" alt="">
            <div class="collapse navbar-collapse" id="navbarSupportedContent">

                <a  href="../../organiser.php" type="submit" class="btn index btn-primary">Retour organiseur</a>

                <form class="d-flex align-items-center">
                    <input class="form-control me-2"
                           type="search"
                           placeholder="Rechercher"
                           aria-label="Search">
                    <button class="btn btn-outline-success"
                            type="submit"><img class="icosearch" src="assets/images/Search-256.png" alt="">
                        </button>

<!--                <button type="button"-->
<!--                        class="btn deconnect btn-primary "-->
<!--                        data-bs-toggle="#"-->
<!--                        data-bs-target="#deconnexion">-->
<!--                </button>-->
                <a href="../../index.php" type="submit" class="btn deconnect btn-primary">Déconnexion</a>
                </form>
            </div>
        </div>
    </nav>
</div>
