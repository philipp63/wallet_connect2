<?php
require_once 'core/function.php';


if (!empty($_POST)) {
    $db = getDatabaseConnexion();
    $errors = array();

    if (empty($_POST['first_name']) || !preg_match('/^[a-zA-Z-]+$/', $_POST['first_name'])) {
        $errors['first_name'] = "Votre prénom n'est pas valide";
    } else {
        if (isset($db)) {
            $req = $db->prepare('SELECT id FROM users WHERE first_name = ?');
            $req->execute([$_POST['first_name']]);
            $user = $req->fetch();
        }
    }

    if (empty($_POST['last_name']) || !preg_match('/^[a-zA-Z-]+$/', $_POST['last_name'])) {
        $errors['last_name'] = "Votre nom n'est pas valide";
    } else {
        if (isset($db)) {
            $req = $db->prepare('SELECT id FROM users WHERE last_name = ?');
            $req->execute([$_POST['last_name']]);
            $user = $req->fetch();
        }
    }

    if (empty($_POST['email']) || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
        $errors['email'] = "Votre email n'est pas valide";
    } else {
        if (isset($db)) {
            $req = $db->prepare('SELECT id FROM users WHERE email = ?');
            $req->execute([$_POST['email']]);
            $user = $req->fetch();
            if ($user) {
                $errors['email'] = 'Cet email est déjà utilisé par un autre compte.';
            }
        }
    }

    if (empty($_POST['password'])) {
        $errors['password'] = "Vous devez rentrer un mot de passe valide";
    }

    if (empty($errors)) {
        if (isset($db)) {
            $req = $db->prepare("INSERT INTO users SET first_name = ?, last_name = ?, email = ?, password = ?");
        }
        $password = password_hash($_POST['password'], PASSWORD_BCRYPT);
        if (!empty($req)) {
            $req->execute([$_POST['first_name'], $_POST['last_name'], $_POST['email'], $password]);
        }
        echo "<script>alert('Votre compte a bien été créé')</script>";
        } else {
        echo "<script>alert('Votre formulaire contient des erreurs, veuillez recliquer sur le bouton Inscription')</script>";
    }
}
?>


<!--Modal Inscription-->
<div class="modal fade"
     id="inscription"
     tabindex="-1"
     aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Inscription</h5>
                <button type="button"
                        class="btn-close"
                        data-bs-dismiss="modal"
                        aria-label="Close">
                </button>
            </div>

            <?php if (!empty($errors)): ?>
                <div class="alert alert-danger">
                    <p>Vous n'avez pas rempli le formulaire correctement! </p>
                    <ul>
                        <?php foreach ($errors as $error): ?>
                            <li><?= $error; ?></li>
                        <?php endforeach; ?>
                    </ul>
                    data-bs-static
                </div>
            <?php endif; ?>

            <div class="modal-body text-lg-start">
                <form method="post" class="row g-3">
                    <div class="row g-2">
                        <div class="col-md-6">
                            <label for="" class="form-label">Prénom</label>
                            <input type="text" class="form-control" aria-label="" name="first_name">
                        </div>

                        <div class="col-md-6">
                            <label for="" >Nom</label>
                            <input type="text" class="form-control" aria-label="" name="last_name">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <label for="" class="form-label">Email</label>
                        <input type="email" class="form-control" aria-label="" name="email">
                    </div>

                    <div class="col-md-6">
                        <label for="" class="form-label">Mot de passe</label>
                        <input type="password" class="form-control" aria-label="" name="password">
                    </div>

                    <div class="col-12">
                        <label for="" class="form-label">Adresse</label>
                        <input type="text" class="form-control" aria-label="" name="address">
                    </div>

                    <div class="col-md-6">
                        <label for="" class="form-label">Ville</label>
                        <input type="text" class="form-control" aria-label="" name="city">
                    </div>

                    <div class="col-md-4">
                        <label for="" class="form-label">Code postal</label>
                        <input type="text" class="form-control" aria-label="" name="zip">
                    </div>

                    <div class="col-12">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" id="invalidCheck"> <!--required-->
                            <label class="form-check-label" for="invalidCheck">
                                Acceptez les termes et les conditions
                            </label>
                            <div class="invalid-feedback">
                                You must agree before submitting.
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <button type="submit" class="btn btn-success">Inscription</button>
<!--                        --><?php //if (!empty($errors)) : ?>
<!--                            <script>-->
<!--                                $(document).ready(function() {-->
<!--                                    $('#inscription').modal('show');-->
<!--                                });-->
<!--                            </script>-->
<!--                        --><?php //endif ?>

                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
