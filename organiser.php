<?php
require_once 'core/function.php';

getHeader('organiser');
?>

<div class="col-12 text-center mt-5">
    <div class="container category col-12  ">
        <div class="row">
            <div class="col-6 align-content-center">

                <div class="card w-75 mb-3">
                    <a href="partials/template/administrationDoc.php" class="button" id="admin">
                    <div class="card-body">
                        <h5 class="card-title">Administration</h5>  <!-- administrationDoc -->
                        <p class="card-text">nombres de document</p>
                        <img src="assets/images/ico-admin.png" alt="">
                    </div>
                    </a>
                </div>

                <div class="card w-75 mb-3">
                    <a href="partials/template/contractsDoc.php" class="button" id="contrat">
                    <div class="card-body">
                        <h5 class="card-title">Contrats</h5> <!-- contractsDoc -->
                        <p class="card-text">nombres de document</p>
                        <img src="assets/images/ico-contrat.png" alt="">
                    </div>
                    </a>
                </div>

                <div class="card w-75 mb-3">
                    <a href="partials/template/lost.php" class="button" id="lost">
                    <div class="card-body">
                        <h5 class="card-title">Document perdu</h5>  <!-- lostDoc -->
                        <p class="card-text">listes des services pour refaire vos document</p>
                        <img src="assets/images/ico-lost.png" alt="">
                    </div>
                    </a>
                </div>

                <div class="card w-75 mb-3">
                    <a href="partials/template/importDoc.php" class="button" id="import">
                    <div class="card-body">
                        <h5 class="card-title">Importer un nouveau document</h5>    <!-- importDoc -->
                        <img src="assets/images/ico-new.png" alt="">
                    </div>
                    </a>
                </div>
            </div>

            <div class="col-6">
                <div class="card w-75 mb-3">
                    <a href="partials/template/invoicesDoc.php" class="button" id="factures">
                    <div class="card-body">
                        <h5 class="card-title">Factures</h5>    <!-- invoicesDoc -->
                        <p class="card-text">nombres de document</p>
                        <img src="assets/images/ico-fact.png" alt="">
                    </div>
                    </a>
                </div>

                <div class="card w-75 mb-3">
                    <a href="partials/template/identityDoc.php" class="button" id="identity">
                    <div class="card-body">
                        <h5 class="card-title">Identité</h5>    <!-- indentityDoc -->
                        <p class="card-text">nombres de document</p>
                        <img src="assets/images/ico-id.png" alt="">
                    </div>
                    </a>
                </div>

                <div class="card w-75 mb-3">
                    <a href="partials/template/insuranceDoc.php" class="button" id="assurance">
                    <div class="card-body">
                        <h5 class="card-title">Assurance</h5>   <!-- insuranceDoc -->
                        <p class="card-text">nombres de document</p>
                        <img src="assets/images/ico-secur.png" alt="">
                    </div>
                    </a>
                </div>

                <div class="card w-75 mb-3">
                    <a href="partials/template/variousDoc.php" class="button" id="divers">
                    <div class="card-body">
                        <h5 class="card-title">Divers</h5>  <!-- variousDoc -->
                        <p class="card-text">nombres de document</p>
                        <img src="assets/images/ico-div.png" alt="">
                    </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>



<?php
getFooter();