<?php
session_start();
require_once $_SERVER['DOCUMENT_ROOT'] .'/vendor/autoload.php';
require_once $_SERVER['DOCUMENT_ROOT'] .'/config.php';

function getHeader(string $title)
{
    include $_SERVER['DOCUMENT_ROOT'] . '/partials/header.php';
}

function getFooter()
{
    include $_SERVER['DOCUMENT_ROOT'] .'./partials/footer.php';
}

function getAdminDoc()
{
    include $_SERVER['DOCUMENT_ROOT'] .'./partials/template/administrationDoc.php';
}
function getCard()
{
    include $_SERVER['DOCUMENT_ROOT'] .'./partials/template/cards.php';
}

/**
 * @return PDO|null
 */
function getDatabaseConnexion(): ?PDO
{
    try {
        $host = 'mysql:host=localhost;dbname='. DB_NAME. ';charset=utf8';
        $db = new PDO($host, DB_USER, DB_PASSWD);
        return $db;
    } catch (Exception $e) {
        return null;
    }
}

function getModalConnection()
{
    include $_SERVER['DOCUMENT_ROOT'] .'./login.php';
}

function getModalInscription()
{
    include $_SERVER['DOCUMENT_ROOT'] .'./inscription.php';
}

function checkLogin(): void
{
    getDatabaseConnexion();
    // Vérifie si l'email est dans la bd: récupérer l'entrée de l'input en PHP $_POST
    // return
}

function uploadDocument() {

// Connexion à la base de données
    getDatabaseConnexion();

    $bdd = 'wallet_connect';

    $sql = "INSERT INTO 'wallet_connect' (`name`,`path`, `size`, `extension`) VALUES (?, ?, ?, ?)";

    try {
        $req = $bdd->prepare($sql);

        // On donne une valeur aux paramètres
        $req->execute(array($name, $path, $size, $extension));

        echo 'Votre document a bien été ajouté ! ';

    } catch (Exception $e) {
        echo '<pre><br>Erreur dans la requête <br/>' . $e->getMessage() . '<br/>';
    }
}


function updateDocument() {

}

function deleteDocument() {

}

/**
 * Better than var_dump
 * @param $var
 */
function dump($var): void
{
    echo '<pre>' . var_export($var, true). '</pre>';
}
